import MyPokemonPage from "./MyPokemonPage";
import PokemonDetailPage from "./PokemonDetailPage";
import PokemonListPage from "./PokemonListPage";

export { MyPokemonPage, PokemonDetailPage, PokemonListPage };

import { Button as MuiButton } from "@mui/material";
import React, { useEffect, useState } from "react";
import Modal from "react-modal";
import { useDispatch, useSelector } from "react-redux";
import { PokemonList } from "../../Components";
import { PokemonTypes } from "../../Redux/Store/Action-types";
import { getPokemonList } from "../../Redux/Store/Actions/Pokemon";
import "../../Styles/Modal.css";
import { history, hp, VerticalGap, wp } from "../../Utilities";
import PokemonDetailPage from "../PokemonDetailPage";

const PokemonListPage = () => {
  const dispatch = useDispatch();
  const { SET_POKEMON_DETAILS } = PokemonTypes;
  const [isOpen, setIsOpen] = useState(false);
  const [url, setUrl] = useState("");
  const [name, setName] = useState("");
  const { ownedPokemonList, listPokemon } = useSelector(
    (state) => state.pokemon
  );

  useEffect(() => {
    dispatch(getPokemonList());
    Modal.setAppElement("#modal");
  }, []);

  const onSeeOwnedList = () => {
    history.push("/my-pokemon");
  };

  const resetPokemonDetails = () => ({
    type: SET_POKEMON_DETAILS,
    payload: {},
  });

  const onPress = (url, name) => {
    setIsOpen(true);
    setName(name);
    setUrl(url);
  };

  const onClose = () => {
    setIsOpen(false);
  };

  return (
    <div id="modal">
      <p css={{ fontSize: "4vmin" }}>
        You have {ownedPokemonList.length} Pokemons
      </p>
      <MuiButton variant="outlined" onClick={onSeeOwnedList}>
        Owned List
      </MuiButton>
      <VerticalGap height={hp(3)} />
      <PokemonList list={listPokemon} onPress={onPress} />
      <VerticalGap height={hp(3)} />
      <Modal
        isOpen={isOpen}
        style={customStyles}
        onRequestClose={(e) => {
          dispatch(resetPokemonDetails());
          e.stopPropagation();
          setIsOpen(false);
        }}
        shouldCloseOnOverlayClick={true}
      >
        <PokemonDetailPage name={name} url={url} onClose={onClose} />
      </Modal>
    </div>
  );
};

export default PokemonListPage;

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    margin: "auto",
    height: hp(70),
    width: wp(90),
    transform: "translate(-50%, -50%)",
    boxShadow: "rgba(149, 157, 165, 0.2) 0px 8px 24px",
    padding: 0,
  },
  overlay: {
    zIndex: 1000,
  },
  tabs: {
    padding: "10%",
  },
};

import { TabContext, TabList, TabPanel } from "@mui/lab";
import { Box, Skeleton, Tab } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Pokeball } from "../../Assets/Images";
import { CatchModal } from "../../Components";
import { About, Details } from "../../Components/Tabs";
import { getPokemonDetails } from "../../Redux/Store/Actions/Pokemon";
import { hp, wp } from "../../Utilities";

const PokemonDetailPage = ({ url, name }) => {
  const { pokemonDetails } = useSelector((state) => state.pokemon);
  const [tab, setTab] = useState(1);
  const [isOpen, setOpen] = useState(false);
  const [success, setSuccess] = useState(false);
  const dispatch = useDispatch();
  const [isPortrait, setIsPortrait] = useState(true);

  useEffect(() => {
    changeOrientation();
    dispatch(getPokemonDetails(url));
  }, []);

  const changeOrientation = () => {
    if (!window.matchMedia("(orientation: portrait)").matches) {
      setIsPortrait(false);
    }
  };

  const getChance = () => {
    return Math.random() < 0.5 ? 0 : 1;
  };

  const handleChange = (event, tab) => {
    setTab(tab);
  };

  const openModal = () => {
    if (getChance() === 0) {
      setSuccess(false);
    } else {
      setSuccess(true);
    }

    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };

  return (
    <div
      style={
        isPortrait
          ? {
              textTransform: "capitalize",
              textAlign: "center",
              display: "flex",
              flexDirection: "column",
              flex: 1,
              fontFamily: "Bauhaus",
              fontSize: "10vmin",
              paddingBottom: hp(3),
            }
          : {
              textTransform: "capitalize",
              textAlign: "center",
              display: "flex",
              flexDirection: "row",
              fontFamily: "Bauhaus",
              fontSize: "10vmin",
              padding: wp(2),
            }
      }
    >
      <div
        style={{
          flex: 10,
          overflow: "hidden",
          display: "block",
          marginLeft: "auto",
          marginRight: "auto",
        }}
      >
        {pokemonDetails.images ? (
          <Carousel
            showThumbs={false}
            interval={5000}
            autoPlay
            transitionTime={1000}
            emulateTouch
            infiniteLoop
            showArrows={false}
            showIndicators={false}
          >
            <div>
              <img
                style={{ width: "75%" }}
                src={pokemonDetails?.images?.front}
              />
            </div>
            <div>
              <img
                style={{ width: "75%" }}
                src={pokemonDetails?.images?.back}
              />
            </div>
          </Carousel>
        ) : (
          <Skeleton
            variant="circular"
            width={100}
            height={100}
            style={{ marginTop: hp(10) }}
          />
        )}
      </div>
      <div
        style={{
          paddding: 0,
          flex: 10,
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
        }}
      >
        <b>{name}</b>
        <Box sx={{ width: "100%", typography: "body1" }}>
          <TabContext value={tab}>
            <Box>
              <TabList
                onChange={handleChange}
                aria-label="lab API tabs example"
                centered
              >
                <Tab label="About" value={1} />
                <Tab label="Abilities & Moves" value={2} />
              </TabList>
            </Box>
            <TabPanel value={1}>
              <About
                speed={pokemonDetails.speed}
                attack={pokemonDetails.attack}
                defense={pokemonDetails.defense}
                hp={pokemonDetails.hp}
                weight={pokemonDetails.weight}
                height={pokemonDetails.height}
                specialAttack={pokemonDetails.specialAttack}
                specialDefense={pokemonDetails.specialDefense}
                species={pokemonDetails.species}
              />
            </TabPanel>
            <TabPanel value={2}>
              <Details
                abilities={pokemonDetails.abilities}
                moves={pokemonDetails.moves}
              />
            </TabPanel>
          </TabContext>
        </Box>
        <img src={Pokeball} style={{ width: wp(15) }} onClick={openModal} />
        <p
          style={{
            fontSize: "6vmin",
          }}
        >
          Catch Me!
          <br />
          <p
            style={{
              fontSize: "3vmin",
            }}
          >
            (Press on the PokeBall)
          </p>
        </p>
        <CatchModal open={isOpen} success={success} onClose={onClose} />
      </div>
    </div>
  );
};

export default PokemonDetailPage;

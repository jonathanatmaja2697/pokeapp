import {
  Button as MuiButton,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import FlatList from "flatlist-react";
import React, { useEffect, useState } from "react";
import { Button, Card } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { PokemonTypes } from "../../Redux/Store/Action-types";
import { colors, hp, VerticalGap, wp, history } from "../../Utilities";

const MyPokemonPage = () => {
  const { SET_OWNDED_POKEMON_LIST } = PokemonTypes;
  const dispatch = useDispatch();
  const { ownedPokemonList } = useSelector((state) => state.pokemon);
  const [open, setOpen] = useState(false);
  const [id, setId] = useState("");
  const [nickname, setNickname] = useState("");
  const theme = useTheme();
  const fullscreen = useMediaQuery(theme.breakpoints.down("xs"));
  const [isPortrait, setIsPortrait] = useState(true);

  useEffect(() => {
    changeOrientation();
  }, []);

  const changeOrientation = () => {
    if (!window.matchMedia("(orientation: portrait)").matches) {
      setIsPortrait(false);
    }
  };

  const setOwnedPokemonList = (payload) => ({
    type: SET_OWNDED_POKEMON_LIST,
    payload,
  });

  const onBack = () => {
    history.push("/");
  };

  const releasePokemon = (id) => {
    const newList = ownedPokemonList.filter((x) => x.id !== id);
    dispatch(setOwnedPokemonList(newList));
    window.location.reload();
  };

  const onClose = () => {
    setOpen(false);
  };

  const renderItem = (item) => {
    return (
      <>
        <Card style={styles.card}>
          <Card.Body style={styles.cardBody(isPortrait)}>
            <div
              style={{
                color: colors.black,
                textTransform: "capitalize",
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-start",
                justifyContent: "center",
              }}
            >
              <img
                src={item.images.front}
                style={isPortrait ? { width: wp(40) } : { width: hp(40) }}
              />
            </div>
            <div
              style={{
                flex: 2,
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-end",
                justifyContent: "center",
              }}
            >
              <div style={{ display: "flex", flexDirection: "column" }}>
                <b
                  style={
                    isPortrait
                      ? {
                          textAlign: "center",
                          margin: 0,
                          fontSize: wp(6),
                        }
                      : {
                          textAlign: "center",
                          margin: 0,
                          fontSize: wp(4),
                        }
                  }
                >
                  {item.nickname}
                </b>

                <p
                  style={{
                    textAlign: "center",
                    margin: 0,
                    fontSize: wp(3),
                    marginBottom: hp(1),
                  }}
                >
                  ({item.name})
                </p>
                <Button
                  style={styles.button}
                  variant="warning"
                  onClick={() => {
                    setOpen(true);
                    setId(item.id);
                    setNickname(item.nickname);
                  }}
                >
                  <b
                    style={{
                      color: colors.white,
                      fontSize: "3.5vmin",
                      alignSelf: "center",
                      fontFamily: "Bauhaus",
                    }}
                  >
                    Release
                  </b>
                </Button>
              </div>
            </div>
          </Card.Body>
        </Card>
        <VerticalGap height={hp(3)} />
      </>
    );
  };

  return (
    <div>
      <MuiButton variant="outlined" onClick={onBack}>
        {"< "}List Page
      </MuiButton>
      <VerticalGap height={hp(4)} />
      <FlatList
        list={ownedPokemonList}
        renderItem={renderItem}
        numColumns={2}
      ></FlatList>
      <Dialog
        open={open}
        onClose={onClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        fullScreen={fullscreen}
      >
        <DialogTitle>Pokemon Release</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Are you sure want to release <b>{nickname}</b>?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <MuiButton onClick={onClose}>Cancel</MuiButton>
          <MuiButton
            onClick={() => {
              releasePokemon(id);
            }}
          >
            Confirm
          </MuiButton>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default MyPokemonPage;

const styles = {
  card: { width: wp(90), borderRadius: 20 },
  cardBody: (isPortrait) =>
    isPortrait
      ? {
          flexDirection: "row",
          display: "flex",
          width: "100%",
          height: hp(18),
          boxShadow: "rgba(149, 157, 165, 0.2) 0px 8px 24px",
          paddingLeft: 0,
        }
      : {
          flexDirection: "row",
          display: "flex",
          width: "100%",
          height: hp(32),
          boxShadow: "rgba(149, 157, 165, 0.2) 0px 8px 24px",
          paddingLeft: 0,
        },
  button: {
    width: wp(20),
    height: hp(5),
    borderRadius: 20,
    display: "flex",
    justifyContent: "center",
    boxShadow:
      "rgba(9, 30, 66, 0.25) 0px 4px 8px -2px, rgba(9, 30, 66, 0.08) 0px 0px 0px 1px",
  },
};

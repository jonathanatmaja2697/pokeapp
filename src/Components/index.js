import TopHeader from "../Components/TopHeader";
import Footer from "./Footer";
import * as Tabs from "./Tabs";
import CatchModal from "./CatchModal";
import PokemonList from "./PokemonList";
import Card from "./Card";

export { TopHeader, Footer, Tabs, CatchModal, PokemonList, Card };

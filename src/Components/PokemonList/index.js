import { Skeleton } from "@mui/material";
import FlatList from "flatlist-react";
import React from "react";
import { Button, Card } from "react-bootstrap";
import { colors, hp, VerticalGap, wp } from "../../Utilities";

const PokemonList = ({ list, onPress }) => {
  const renderItem = (item) => {
    return (
      <>
        <Card style={styles.card}>
          <Card.Body style={styles.cardBody}>
            <div
              style={{
                color: colors.black,
                textTransform: "capitalize",
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-start",
                justifyContent: "center",
                marginLeft: wp(3),
                flex: 1,
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "center",
                  alignContent: "center",
                }}
              >
                {item.images.back_default ? (
                  <img src={item.images.back_default} style={styles.image} />
                ) : (
                  <Skeleton />
                )}
                {item.name ? (
                  <p
                    style={{
                      textAlign: "center",
                      margin: 0,
                      fontSize: "6vmin",
                      alignSelf: "center",
                    }}
                  >
                    {item.name}
                  </p>
                ) : (
                  <Skeleton />
                )}
              </div>
            </div>
            <div
              style={{
                flex: 2,
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-end",
                justifyContent: "flex-end",
              }}
            >
              <Button
                variant="warning"
                style={{
                  width: wp(20),
                  height: hp(5),
                  borderRadius: 20,
                  display: "flex",
                  justifyContent: "center",
                  boxShadow:
                    "rgba(9, 30, 66, 0.25) 0px 4px 8px -2px, rgba(9, 30, 66, 0.08) 0px 0px 0px 1px",
                }}
                onClick={() => onPress(item.url, item.name)}
              >
                <b
                  style={{
                    color: colors.white,
                    fontSize: "3.5vmin",
                    alignSelf: "center",
                    fontFamily: "Bauhaus",
                  }}
                >
                  Detail
                </b>
              </Button>
            </div>
          </Card.Body>
        </Card>
        <VerticalGap height={hp(3)} />
      </>
    );
  };

  const renderEmptyComponent = () => {
    const emptyItems = [];

    for (let i = 0; i < 5; i++) {
      emptyItems.push(
        <>
          <Card style={styles.card}>
            <Card.Body style={styles.cardBody}>
              <div
                style={{
                  color: colors.black,
                  textTransform: "capitalize",
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-start",
                  justifyContent: "center",
                  marginLeft: wp(3),
                  flex: 1,
                }}
              >
                <Skeleton width={wp(45)} />
              </div>
              <div
                style={{
                  flex: 2,
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-end",
                  justifyContent: "flex-end",
                }}
              >
                <Skeleton width={wp(20)} />
              </div>
            </Card.Body>
          </Card>
          <VerticalGap height={hp(3)} />
        </>
      );
    }
    return <div>{emptyItems}</div>;
  };

  return (
    <FlatList
      list={list}
      renderItem={renderItem}
      renderWhenEmpty={() => renderEmptyComponent()}
    />
  );
};

export default PokemonList;

const styles = {
  card: { width: wp(90), borderRadius: 20 },
  cardBody: {
    flexDirection: "row",
    display: "flex",
    width: "100%",
    height: hp(15),
    boxShadow: "rgba(149, 157, 165, 0.2) 0px 8px 24px",
  },
  image: {
    width: hp(12),
    height: hp(12),
  },
};

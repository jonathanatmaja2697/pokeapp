import { Skeleton } from "@mui/material";
import React from "react";
import { Table } from "react-bootstrap";
import { wp } from "../../../Utilities";

const About = ({
  species = "",
  height = "",
  weight = "",
  hp = "",
  attack = "",
  defense = "",
  specialAttack = "",
  specialDefense = "",
  speed = "",
}) => {

  return (
    <div>
      <Table borderless>
        <tbody>
          <tr>
            <td style={styles.left}>
              <b>Species</b>
            </td>
            <td></td>
            <td style={styles.right}>
              {species ? species : <Skeleton style={styles.skeleton} />}
            </td>
          </tr>
          <tr>
            <td style={styles.left}>
              <b>Height</b>
            </td>
            <td></td>
            <td style={styles.right}>
              {height ? height : <Skeleton style={styles.skeleton} />}
            </td>
          </tr>
          <tr>
            <td style={styles.left}>
              <b>Weight</b>
            </td>
            <td></td>
            <td style={styles.right}>
              {weight ? weight : <Skeleton style={styles.skeleton} />}
            </td>
          </tr>
          <tr>
            <td style={styles.left}>
              <b>Hp</b>
            </td>
            <td></td>
            <td style={styles.right}>
              {hp ? hp : <Skeleton style={styles.skeleton} />}
            </td>
          </tr>
          <tr>
            <td style={styles.left}>
              <b>Attack</b>
            </td>
            <td></td>
            <td style={styles.right}>
              {attack ? attack : <Skeleton style={styles.skeleton} />}
            </td>
          </tr>
          <tr>
            <td style={styles.left}>
              <b>Special Attack</b>
            </td>
            <td></td>
            <td style={styles.right}>
              {specialAttack ? (
                specialAttack
              ) : (
                <Skeleton style={styles.skeleton} />
              )}
            </td>
          </tr>
          <tr>
            <td style={styles.left}>
              <b>Defense</b>
            </td>
            <td></td>
            <td style={styles.right}>
              {defense ? defense : <Skeleton style={styles.skeleton} />}
            </td>
          </tr>
          <tr>
            <td style={styles.left}>
              <b>Special Defense</b>
            </td>
            <td></td>
            <td style={styles.right}>
              {specialDefense ? (
                specialDefense
              ) : (
                <Skeleton style={styles.skeleton} />
              )}
            </td>
          </tr>
          <tr>
            <td style={styles.left}>
              <b>Speed</b>
            </td>
            <td></td>
            <td style={styles.right}>
              {speed ? speed : <Skeleton style={styles.skeleton} />}
            </td>
          </tr>
        </tbody>
      </Table>
    </div>
  );
};

export default About;

const styles = {
  left: {
    textAlign: "left",
    fontSize: '4vmin',
  },
  right: {
    textAlign: "right",
    fontSize: '4vmin',
  },
  skeleton: {
    position: "relative",
    left: "auto",
    marginLeft: "auto",
    width: wp(25),
  },
};

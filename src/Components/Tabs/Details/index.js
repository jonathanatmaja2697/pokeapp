import React from "react";
import { Table } from "react-bootstrap";
import { Skeleton } from "@mui/material";
import { wp } from "../../../Utilities";

const Details = ({ abilities, moves }) => {
  return (
    <div>
      <Table borderless>
        <tbody>
          <tr>
            <td style={styles.left}>
              <b>Abilities</b>
            </td>
            <td></td>
            <td style={styles.right}>
              {abilities ? abilities : <Skeleton style={styles.skeleton} />}
            </td>
          </tr>
          <tr>
            <td style={styles.left}>
              <b>Moves</b>
            </td>
            <td></td>
            <td style={styles.right}>
              {moves ? moves : <Skeleton style={styles.skeleton} />}
            </td>
          </tr>
        </tbody>
      </Table>
    </div>
  );
};

export default Details;
const styles = {
  left: {
    textAlign: "left",
    fontSize: "4vmin",
  },
  right: {
    textAlign: "right",
    fontSize: "4vmin",
  },
  skeleton: {
    position: "relative",
    left: "auto",
    marginLeft: "auto",
    width: wp(25),
  },
};


/** @jsxRuntime classic */
/** @jsx jsx */

import { LoadingButton } from "@mui/lab";
import { TextField, Typography } from "@mui/material";
import { nanoid } from "nanoid";
// eslint-disable-next-line no-unused-vars
import React, { useEffect, useState } from "react";
import Modal from "react-modal";
import { useDispatch, useSelector } from "react-redux";
import { PokeballAnimated, BackIcon } from "../../Assets/Images";
import { PokemonTypes } from "../../Redux/Store/Action-types";
import { hp, useDebounce, wp } from "../../Utilities";
import { jsx } from "@emotion/react";
import styled from "@emotion/styled";

const CatchModal = ({ open, onClose, success = false }) => {
  const dispatch = useDispatch();
  const [nickname, setNickname] = useState("");
  const [loading, setLoading] = useState(false);
  const { SET_OWNDED_POKEMON_LIST } = PokemonTypes;
  const [disabled, setDisabled] = useState(false);
  const { ownedPokemonList, pokemonDetails } = useSelector(
    (state) => state.pokemon
  );
  const [isPortrait, setIsPortrait] = useState(true);
  const debounceNickname = useDebounce(nickname, 500);

  const setOwnedPokemonList = (payload) => ({
    type: SET_OWNDED_POKEMON_LIST,
    payload,
  });

  useEffect(() => {
    changeOrientation();
  }, []);
  
  useEffect(() => {
    if (debounceNickname) {
      const whitelist = ownedPokemonList.filter((x) => x.nickname === nickname);
      setDisabled(false);
      if (whitelist.length > 0) {
        setDisabled(true);
      }
    } else {
      setNickname("");
    }
  }, [debounceNickname]);

  const changeOrientation = () => {
    if (!window.matchMedia("(orientation: portrait)").matches) {
      setIsPortrait(false);
    }
  };

  const onChange = (e) => {
    setNickname(e.target.value);
  };

  const onSubmit = () => {
    const newList = ownedPokemonList;
    newList.push({
      ...pokemonDetails,
      nickname: nickname,
      id: nanoid(),
    });

    setLoading(true);

    setTimeout(() => {
      dispatch(setOwnedPokemonList(newList));
      window.location.reload();
    }, 1500);
  };

  const LoadingButtons = styled(LoadingButton)({
    width: wp(18),
    height: hp(6),
    fontSize: wp(1.8),
  });

  return (
    <div>
      <Modal
        isOpen={open}
        style={customStyles(isPortrait)}
        onRequestClose={(e) => {
          e.stopPropagation();
          onClose();
        }}
        shouldCloseOnOverlayClick={true}
      >
        {success === true ? (
          <>
            <img
              src={PokeballAnimated}
              style={{
                width: wp(30),
              }}
            />
            <Typography id="transition-modal-title" variant="h6" component="h2">
              Congratulations!<br></br>
              You've Got
              <b style={{ fontFamily: "Bauhaus" }}>{pokemonDetails.name}</b> !
            </Typography>
            <Typography
              id="transition-modal-description"
              sx={{ mt: 2, fontSize: "4vmin" }}
            >
              You have to give it a <b>unique</b> nickname, or you gonna lose it
              !
            </Typography>
            <TextField
              id="standard-basic"
              variant="standard"
              value={nickname}
              onChange={onChange}
              style={{
                display: "flex",
                alignSelf: "center",
                marginTop: hp(4),
                // marginBottom: hp(4),
                textAlign: "center",
                marginBottom: hp(2),
              }}
            />
            <LoadingButtons
              loading={loading}
              variant="outlined"
              onClick={onSubmit}
              disabled={disabled}
            >
              Submit
            </LoadingButtons>
          </>
        ) : (
          <>
            <img
              src={PokeballAnimated}
              style={{
                width: wp(30),
              }}
            />
            <p style={{ fontSize: "4vmin" }}>
              My bad! You failed to catch <br />
              <b style={{ fontSize: "9vmin" }}>{pokemonDetails.name}</b>
            </p>
            <img
              src={BackIcon}
              style={style.backBtn(isPortrait)}
              onClick={onClose}
            />
          </>
        )}
      </Modal>
    </div>
  );
};

export default CatchModal;
const style = {
  backBtn: (isPortrait) => ({
    width: isPortrait === "landscape-primary" ? wp(6) : wp(10),
  }),
};

const customStyles = (isPortrait) => ({
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    margin: "auto",
    height: hp(80),
    width: wp(90),
    transform: "translate(-50%, -50%)",
    boxShadow: "rgba(149, 157, 165, 0.2) 0px 8px 24px",
    padding: 0,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    textAlign: "center",
    paddingBottom: hp(3),
    paddingLeft: wp(10),
    paddingRight: wp(10),
    justifyContent: isPortrait ? "center" : undefined,
  },
  overlay: {
    zIndex: 1000,
  },
  tabs: {
    padding: "10%",
  },
});

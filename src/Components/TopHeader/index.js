import React from "react";
import { PokemonIcon } from "../../Assets/Images";
import "../../Styles/TopHeader.css";
import { Box } from "@mui/material";
import { colors, hp, wp } from "../../Utilities";

const TopHeader = () => {
  return (
    <div className="Top-header">
      <Box sx={styles.box}>
        <img src={PokemonIcon} className="icon" style={{ height: hp(9) }} />
      </Box>
    </div>
  );
};

export default TopHeader;

const styles = {
  box: {
    height: hp(15),
    width: wp(100),
    backgroundColor: colors.yellow,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    position: "fixed",
    zIndex: 1,
    top: 0,
    right: 0,

    padding: hp(2),
  },
};

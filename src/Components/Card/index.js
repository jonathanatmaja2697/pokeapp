import React from "react";
import { hp, wp } from "../../Utilities";

const Card = ({ name }) => {
  return <div style={style.card}>{name}</div>;
};

export default Card;

const style = {
  card: {
    width: wp(10),
    height: hp(10),
    borderWidth: 1,
  },
};

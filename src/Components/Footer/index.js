import { Box } from "@mui/material";
import React from "react";
import "../../Styles/TopHeader.css";
import { colors, hp, wp } from "../../Utilities";

const Footer = () => {
  return (
    <div>
      <Box sx={styles.box}></Box>
    </div>
  );
};

export default Footer;

const styles = {
  box: {
    height: hp(10),
    width: wp(100),
    backgroundColor: colors.yellow,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    position: "fixed",
    bottom: 0,
    left: 0,
  },
};

import PokemonIcon from "./pokemon.png";
import BackIcon from "./back.png";
import Pokeball from "./pokeball.png";
import PokeballAnimated from "./pokeball-animated.gif";
import HomeIcon from "./home.png";
import PokeballTransparent from "./pokeball-transparent.png";

export {
  PokemonIcon,
  BackIcon,
  Pokeball,
  PokeballAnimated,
  PokeballTransparent,
  HomeIcon
};

import { useEffect, useState } from "react";

const wp = (val) => {
  return (window.innerWidth * val) / 100 + "px";
};

const hp = (val) => {
  return (window.innerHeight * val) / 100 + "px";
};

const pokemonDetailMapper = (data) => {
  const abilities = [];
  const moves = [];

  data.abilities.map((x) => {
    abilities.push(x.ability.name);
  });

  data.moves.map((x) => {
    moves.push(x.move.name);
  });

  return {
    name: data.name,
    images: {
      front: data?.sprites?.front_default,
      back: data?.sprites?.back_default,
    },
    abilities: abilities.join(", "),
    moves: moves.join(", "),
    height: data.height,
    weight: data.weight,
    species: data.species.name,
    hp:
      data?.stats.filter((stat) => stat.stat.name === "hp")[0]?.base_stat || 0,
    attack:
      data?.stats.filter((stat) => stat.stat.name === "attack")[0]?.base_stat ||
      0,
    defense:
      data?.stats.filter((stat) => stat.stat.name === "defense")[0]
        ?.base_stat || 0,
    specialAttack:
      data?.stats.filter((stat) => stat.stat.name === "special-attack")[0]
        ?.base_stat || 0,
    specialDefense:
      data?.stats.filter((stat) => stat.stat.name === "special-defense")[0]
        ?.base_stat || 0,
    speed:
      data?.stats.filter((stat) => stat.stat.name === "speed")[0]?.base_stat ||
      0,
  };
};

const useDebounce = (value, delay) => {
  const [debouncedValue, setDebouncedValue] = useState(value);
  useEffect(() => {
    const handler = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);

    return () => {
      clearTimeout(handler);
    };
  }, [value, delay]);
  return debouncedValue;
};

export { wp, hp, pokemonDetailMapper, useDebounce };

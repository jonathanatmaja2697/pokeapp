import React from "react";

const VerticalGap = ({ height }) => {
  return <div className="gap-height" style={{ height }}></div>;
};

const HorizontalGap = ({ width }) => {
  return <div className="gap-width" style={{ width }}></div>;
};

export { VerticalGap, HorizontalGap };

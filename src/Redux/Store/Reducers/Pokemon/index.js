import { PokemonTypes } from "../../Action-types";
import { SET_POKEMON_DETAILS } from "../../Action-types/Pokemon";

const { SET_POKEMON_LIST, SET_PAGE_INDEX, SET_OWNDED_POKEMON_LIST } =
  PokemonTypes;

const initialState = {
  listPokemon: [],
  pageIndex: "0",
  ownedPokemonList: [],
  pokemonDetails: {},
};

const reducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case SET_POKEMON_LIST:
      return { ...state, listPokemon: payload };
    case SET_PAGE_INDEX:
      return { ...state, pageIndex: payload };
    case SET_OWNDED_POKEMON_LIST:
      return { ...state, ownedPokemonList: payload };
    case SET_POKEMON_DETAILS:
      return { ...state, pokemonDetails: payload };
    default:
      return state;
  }
};

export default reducer;

import Services from "../../../../Services";
import { pokemonDetailMapper } from "../../../../Utilities/Functions";
import { PokemonTypes } from "../../Action-types";

const { SET_POKEMON_DETAILS, SET_POKEMON_LIST } = PokemonTypes;

const setPokemonList = (payload) => ({
  type: SET_POKEMON_LIST,
  payload,
});

const setPokemonDetails = (payload) => ({
  type: SET_POKEMON_DETAILS,
  payload,
});

const getPokemonList = () => (dispatch) => {
  return new Promise((resolve, reject) => {
    Services.Get("/v2/pokemon")
      .then((res) => {
        let finalData = [];

        for (let i = 0; i < res.data.results.length; i++) {
          Services.Get(res.data.results[i].url).then((detailRes) => {
            finalData.push({
              name: res.data.results[i].name,
              url: res.data.results[i].url,
              id: i + 1,
              images: detailRes.data.sprites,
            });
            if (finalData.length === res.data.results.length) {
              dispatch(setPokemonList(finalData));
              resolve(res.data);
            }
          });
        }
      })
      .catch((err) => {
        reject(err);
      });
  });
};

const getPokemonDetails = (url) => (dispatch) => {
  return new Promise((resolve, reject) => {
    Services.Get(url)
      .then((res) => {
        dispatch(setPokemonDetails(pokemonDetailMapper(res.data)));
        resolve(res.data);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

export { getPokemonDetails, getPokemonList };

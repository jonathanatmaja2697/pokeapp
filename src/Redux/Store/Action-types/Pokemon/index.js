const SET_POKEMON_LIST = "@pokemon/set-pokemon-list";
const SET_PAGE_INDEX = "@pokemon/set-page-index";
const SET_OWNDED_POKEMON_LIST = "@pokemon/set-owned-pokemon-list";
const SET_POKEMON_DETAILS = "@pokemon/set-pokemon-details";

export {
  SET_POKEMON_LIST,
  SET_PAGE_INDEX,
  SET_OWNDED_POKEMON_LIST,
  SET_POKEMON_DETAILS,
};

import thunk from "redux-thunk";
import storage from "redux-persist/lib/storage";
import { combineReducers, createStore, applyMiddleware, compose } from "redux";
import { persistReducer, persistStore } from "redux-persist";
import { PokemonReducers } from "./Reducers";

const appReducer = combineReducers({
  pokemon: PokemonReducers,
});

const rootReducer = (state, action) => {
  // when a logout action is dispatched it will reset redux state

  return appReducer(state, action);
};

export const configureStore = () => {
  const persistConfig = {
    key: "root",
    storage,
    whitelist: ["pokemon"],
  };

  const persistedReducer = persistReducer(persistConfig, rootReducer);
  const store = createStore(persistedReducer, compose(applyMiddleware(thunk)));
  const persistor = persistStore(store);

  return {
    store,
    persistor,
  };
};

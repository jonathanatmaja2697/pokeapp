import axios from "axios";
import { withRouter } from "react-router-dom";

const Get = (path, request) => {
  const req = [];
  if (request) {
    Object.keys(request).forEach((item) => {
      req.push(`${[item]}=${request[item]}`);
    });
  }
  const promise = new Promise((resolve, reject) => {
    axios({
      method: "get",
      baseURL: `${process.env.REACT_APP_API_URL}`,
      url: `${path}?${req.join("&") || ""}`,
    }).then(
      (res) => {
        resolve(res);
      },
      (err) => {
        reject(err);
      }
    );
  });
  return promise;
};

const Services = {
  Get,
};

export default withRouter(Services);

import { MyPokemonPage, PokemonDetailPage, PokemonListPage } from "../../Pages";

export const routeSources = [
  { component: PokemonListPage, path: "/", exact: true, key: "LIST" },
  { component: PokemonDetailPage, path: "/detail", exact: true, key: "DETAIL" },
  {
    component: MyPokemonPage,
    path: "/my-pokemon",
    exact: true,
    key: "MYPOKEMON",
  },
  {
    //redirect to list page
    component: PokemonListPage,
    path: "/404-not-found",
    exact: true,
    key: "PAGENOTFOUND",
    title: "404 Page Not Found",
  },
];

import React from "react";
import { Route } from "react-router-dom";
import { Footer, TopHeader } from "../../Components";
import "../../Styles/App.css";
import { hp } from "../../Utilities";

export const AuthRoute = ({ component, layout: Layout, ...rest }) => {
  return (
    <div className="App">
      <header
        className="App-header"
        style={{ marginTop: hp(18), marginBottom: hp(12) }}
      >
        <TopHeader />
        <Route
          {...rest}
          exact
          render={(props) => {
            if (Layout) {
              return <Layout>{React.createElement(component, props)}</Layout>;
            }
            return React.createElement(component, props);
          }}
        />
        <Footer />
      </header>
    </div>
  );
};

# POKEAPP

## Steps to install
1. Clone this repository to your local machine 
    > git clone https://gitlab.com/jonathanatmaja2697/pokeapp.git
2. Open terminal at project root, and run **yarn install**.
3. create **.env** file at project root, and insert 
    > REACT_APP_API_URL = "https://pokeapi.co/api"
4. Start development server using **yarn start**. Your browser will automatically open the localhost site.


## Install on Android / Windows / Mac (Options)
1. Open https://sharp-bhabha-e6e998.netlify.app/
2. On Windows / Mac you will see this prompt
<br>
<img src="./public/prompt-mac-windows.png" width="400">
<br>
3. On Android you can install using Chrome (recommended), and follow this step
<br>
<img src="./public/prompt-android-1.jpg" width="300">
<br>
<img src="./public/prompt-android-2.jpg" width="300">


### Thank you